#!/usr/bin/env bash

#################################################################
# This script is meant to be run on fabiserv2.up.ac.za          #
# OS: Debian 11 (bullseye) amd64                                #
# Script written by Albe van der Merwe                          #
#################################################################

command -v tantan >/dev/null 2>&1 || {
    echo >&2 "I require tantan but it's not installed. Aborting.";
    exit 1;
}
command -v RepeatMasker >/dev/null 2>&1 || {
    echo >&2 "I require RepeatMasker but it's not installed. Aborting.";
    exit 1;
}
command -v augustus >/dev/null 2>&1 || {
    echo >&2 "I require augustus but it's not installed. Aborting.";
    exit 1;
}
command -v busco >/dev/null 2>&1 || {
    echo >&2 "I require busco but it's not installed. Aborting.";
    exit 1;
}

echo "Making a symlink to the genome file..."
ln -snf *.xz genome.fna.xz

mkdir 01_softmask && cd 01_softmask
echo "Running (01.1) Tantan 23 masker..."
cp ../genome.fna.xz . && unxz *.xz
tantan -f 0 genome.fna > genome_tantan_softmask.fna
#RepeatMasker genome_tantan_softmasked.fna -species fungi -norna -pa 6 -gccalc -s -xsmall -poly -gff -nolow
cd ..
echo "...DONE"

mkdir 02_annotation && cd 02_annotation
echo "Running (02.1) Augustus 3.4.0..."
cp ../01_softmask/genome_tantan_softmask.fna .
augustus --strand=both --progress=true --gff3=on --uniqueGeneId=true --species=neurospora_crassa genome_tantan_softmask.fna > genome_augustus_annotation.gff3
echo "...DONE"

mkdir 03_busco && cd 03_busco
echo "Running (03.1) BUSCO 5.0.0..."
cp ../01_softmask/genome_tantan_softmask.fna .
busco -i genome_tantan_softmask.fna -c 6 -m geno -l fungi_odb10 -f --update-data --out output
cd ..
echo "...DONE"

echo "Removing symlink..."
rm -rf genome.fna.xz
echo "...DONE"

# Now do the cleanup and zipping
#echo "Cleaning up..."
#rm -rf 01_softmask/genome.fna
#xz -9 01_softmask/genome_tantan_softmask.fna
#rm -rf 02_annotation/genome_tantan_softmask.fna
#xz -9 02_annotation/genome_augustus_annotation.gff3
#rm -rf 03_busco/busco_downloads
#rm -rf 03_busco/genome_tantan_softmask.fna
#cp 03_busco/output/*.txt 03_busco
#cd 03_busco
#XZ_OPT=-9 tar cvfJ busco_sequences.txz output/run*/busco_sequences
#rm -rf output
#cd ..
#echo "...DONE"
